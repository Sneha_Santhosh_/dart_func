void main() {
  print('Hello world!');

  sum();
  sumParams(2, 10);
  sumReturn(3, 4);
  sumRequiredParam(firstNum: 10, secondNum: 200);
}

void sum() {
  print(3 + 5);
}

void sumParams(int a, int b) {
  print('${a + b}');
}

int sumReturn(int a, int b) {
  return (a + b);
}

void sumRequiredParam({required int firstNum, required int secondNum}) {
  print(firstNum + secondNum);
}
